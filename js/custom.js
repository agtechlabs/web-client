$(document).ready(function () {
    var trigger = $('.hamburger'),
        overlay = $('.overlay'),
        accountHover     = $('#account-nav'),
        manufacturerHover = $('#manufacturer-nav'),
        customerHover    = $('#customer-nav'),
        isClosed = false;

    function buttonSwitch() {

        if (isClosed === true) {
            overlay.hide();
            trigger.removeClass('is-open');
            trigger.addClass('is-closed');
            accountHover.show();
            manufacturerHover.show();
            customerHover.show();

            isClosed = false;
        } else {
            overlay.show();
            trigger.removeClass('is-closed');
            trigger.addClass('is-open');
            accountHover.hide();
            manufacturerHover.hide();
            customerHover.hide();
            isClosed = true;
        }
    }

    trigger.click(function () {
        buttonSwitch();
        $('#account-nav').removeClass('toggled');
        $('#manufacturer-nav').removeClass('toggled');
        $('#customer-nav').removeClass('toggled');
    });

    $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
    });

    $('.ag-account-hover').mouseover(function(){
      $('#account-nav').toggleClass('toggled');
    });

    $('.ag-account-hover').mouseout(function(){
      $('#account-nav').removeClass('toggled');
    });
    $('.ag-manufacturer-hover').mouseover(function(){
      $('#manufacturer-nav').toggleClass('toggled');
    });

    $('.ag-manufacturer-hover').mouseout(function(){
      $('#manufacturer-nav').removeClass('toggled');
    });
    $('.ag-customer-hover').mouseover(function(){
      $('#customer-nav').toggleClass('toggled');
    });

    $('.ag-customer-hover').mouseout(function(){
      $('#customer-nav').removeClass('toggled');
    });

    $('#account-datatable').DataTable();
});
